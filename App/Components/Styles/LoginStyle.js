import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  top: {
    flex: 1,
    backgroundColor: "#311b92",
    justifyContent: "center",
    alignItems: "center"
  },
  bottom: {
    flex: 1,
    backgroundColor: "#311b92"
  },
  texthead: {
    fontFamily: "AppleSDGothicNeo-Light",
    fontSize: 30,
    color: "white"
  }
});
