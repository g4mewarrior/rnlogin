import React from "react";
import { Text, View } from "react-native";
import styles from "./Styles/LoginStyle";

export default class Login extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <Text style={styles.texthead}> TOP </Text>
        </View>
        <View style={styles.bottom}>
          <Text>Bottom </Text>
        </View>
      </View>
    );
  }
}
