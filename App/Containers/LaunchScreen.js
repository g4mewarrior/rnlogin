import React from "react";
import { View } from "react-native";
import Login from "../Components/Login";
export default class LaunchScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Login />
      </View>
    );
  }
}
