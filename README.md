### **RNLogin** ###

A react-native Login page for iOS and Android apps.
Initially i am just trying to make a Component out of it so as to convert it into an NPM package to be used by others.
Feel free to clone it and Create pull requests if you have any suggestions.

How to use :-
1. Clone the repo.
2. cd /path/to/directory
3. npm install

For iOS :- 
4. react-native run-ios

For Android :- 
4. react-native run-android